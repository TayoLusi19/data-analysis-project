[Web, Social Media, Logs] --> |Kinesis Data Streams|
                               ↓
                             |Kinesis Firehose| --> |S3 Data Lake| --> |Glue Catalog|
                             ↓                                          ↓
                     [Real-time Analytics]                         [ETL Jobs]
                             ↓                                          ↓
                  |Amazon MSK (Kafka)| <---> |AWS Lambda| <---> |Athena/Presto Queries|
                             ↓                                          ↓
                     [Processed Data]                           [Insights & Reports]
                             ↓                                          ↓
                   |Business Applications|                   |Data Visualization Tools|
